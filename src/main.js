// Importar Librerías
import Vue from 'vue'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueSelect from 'vue-select'
import VueRouter from 'vue-router'
import DatePicker from 'vue2-datepicker'

// Importar Componentes
import App from './App.vue'
import TopNav from './components/TopNav.vue'
import TabsNav from './components/TabsNav.vue'
import Home from './components/tabs/Home.vue'
import Notify from './components/Notify.vue'
import Students from './components/Students.vue'
import Sessions from './components/tabs/Sessions.vue'
import Activities from './components/Activities.vue'
import Activity from './components/Activity.vue'

Vue.config.productionTip = false;

// Inicialización de Librerías
Vue.use(VueMaterial);
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(DatePicker);

// Rutas
const routes = [
  {path: '/', redirect: {name: "Home"}},
  {path: '/inicio/', name: 'Home', component: Home},
  // {path: '/social', name: 'Social', component: Social},
  // {path: '/progreso', name: 'restaurante-destacado', component: RestauranteTop},
  // {path: '/progreso', name: 'Progress', component: Progress},
  {path: '/sesiones/', name:'Sessions', component: Sessions},
  {path: '/sesiones/:sessionid/actividades/', name: 'Activities', component: Activities},
  {path: '/sesiones/:sessionid/actividades/:activityid', name: 'Activity', component: Activity},
];

const router = new VueRouter({
  routes,
  mode: 'history'
})

// Inicialización de Componentes
Vue.component('v-select', VueSelect)
Vue.component('top-nav', TopNav);
Vue.component('tabs-nav', TabsNav);
Vue.component('home', Home);
Vue.component('notify', Notify);
Vue.component('students', Students);
// Vue.component('sessions', Sessions);
// Vue.component('session', Activities);
// Vue.component('activity', Activity);

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
